import kivy

import os
import sys
from kivy.animation import Animation
from kivy.app import App
from kivy.factory import Factory
from kivy.lang import Parser, Builder, ParserException
from kivy.properties import ObjectProperty, Clock
from kivy.uix.codeinput import CodeInput
from kivy.uix.screenmanager import ScreenManager, Screen, WipeTransition
from kivy.uix.boxlayout import BoxLayout

kivy.require('1.9.1')

'''Liste des classes a instancier depuis les fichiers .kv
'''
ROOT = os.path.dirname(__file__)
COMP_KVS = os.path.join(ROOT, 'vues/scenarios')
OPT_KVS = os.path.join(ROOT, 'vues/options')
COMP_CLASSES = [c[:-3] for c in os.listdir(COMP_KVS)
                if c.endswith('.kv')]
OPT_CLASSES = [c[:-3] for c in os.listdir(OPT_KVS)
               if c.endswith('.kv')]


class KivyRenderTextInput(CodeInput):
    def keyboard_on_key_down(self, window, keycode, text, modifiers):
        is_osx = sys.platform == 'darwin'
        # Keycodes on OSX:
        ctrl, cmd = 64, 1024
        key, key_str = keycode

        if text and not key in (list(self.interesting_keys.keys()) + [27]):
            # This allows *either* ctrl *or* cmd, but not both.
            if modifiers == ['ctrl'] or (is_osx and modifiers == ['meta']):
                if key == ord('s'):
                    self.catalog.change_kv(True)
                    return

        return super(KivyRenderTextInput, self).keyboard_on_key_down(
                window, keycode, text, modifiers)


class MenuScreen(Screen):
    pass


class AboutScreen(Screen):
    about = ObjectProperty()

    def __init__(self, **kwargs):
        super(AboutScreen, self).__init__(**kwargs)
        with open(os.path.join(ROOT, 'vues/about.txt'), 'rb') as file:
            self.about.text = file.read().decode('utf8')

    pass


class StartScreen(Screen):
    language_box = ObjectProperty()
    screen_manager = ObjectProperty()
    screen_option = ObjectProperty()

    def __init__(self, **kwargs):
        self._previously_parsed_text = ''
        super(StartScreen, self).__init__(**kwargs)
        self.clean_screen()

    def clean_screen(self):
        self.show_kv(None, "Bienvenue")
        self.carousel = None

    def show_kv(self, instance, value):
        self.screen_manager.current = value
        self.screen_option.current = value
        child = self.screen_manager.current_screen.children[0]
        with open(child.kv_file, 'rb') as file:
            self.language_box.text = file.read().decode('utf8')
        Clock.unschedule(self.change_kv)
        self.change_kv()
        # reset undo/redo history
        self.language_box.reset_undo()

    def change_kv(self, *largs):
        txt = self.language_box.text
        if self.screen_manager.current_screen is not None:
            kv_container = self.screen_manager.current_screen.children[0]
            try:
                parser = Parser(content=txt)
                kv_container.clear_widgets()
                widget = Factory.get(parser.root.name)()
                Builder._apply_rule(widget, parser.root, parser.root)
                kv_container.add_widget(widget)
            except (SyntaxError, ParserException) as e:
                self.show_error(e)
            except Exception as e:
                self.show_error(e)

    def reset_kv(self, *largs):
        if self.screen_manager.current_screen is not None:
            self.show_kv(None, self.screen_manager.current);

    def show_error(self, e):
        self.info_label.text = str(e)
        self.anim = Animation(top=190.0, opacity=1, d=2, t='in_back') + \
                    Animation(top=190.0, d=3) + \
                    Animation(top=0, opacity=0, d=2)
        self.anim.start(self.info_label)

    pass


class Container(BoxLayout):
    def __init__(self, **kwargs):
        super(Container, self).__init__(**kwargs)
        self.previous_text = open(self.kv_file).read()
        parser = Parser(content=self.previous_text)
        widget = Factory.get(parser.root.name)()
        Builder._apply_rule(widget, parser.root, parser.root)
        self.add_widget(widget)

    @property
    def kv_file(self):
        return os.path.join(COMP_KVS, self.__class__.__name__ + '.kv')


for class_name in COMP_CLASSES:
    globals()[class_name] = type(class_name, (Container,), {})


class Infos(BoxLayout):
    def __init__(self, **kwargs):
        super(Infos, self).__init__(**kwargs)
        self.previous_text = open(self.opt_file).read()
        parser = Parser(content=self.previous_text)
        widget = Factory.get(parser.root.name)()
        Builder._apply_rule(widget, parser.root, parser.root)
        self.add_widget(widget)

    @property
    def opt_file(self):
        return os.path.join(OPT_KVS, self.__class__.__name__ + '.kv')


for class_name in OPT_CLASSES:
    globals()[class_name] = type(class_name, (Infos,), {})


class MenuApp(App):
    kv_directory = 'vues'

    def build(self):
        sm = ScreenManager(transition=WipeTransition())
        sm.add_widget(MenuScreen(name='menu'))
        sm.add_widget(AboutScreen(name='about'))
        sm.add_widget(StartScreen(name='start'))
        return sm


if __name__ == '__main__':
    MenuApp().run()
